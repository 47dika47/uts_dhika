import 'package:flutter/material.dart';
import './textoutput.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tugas UTS Flutter',
      home: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          leading: new Icon(Icons.home),
          title: Text('Tugas UTS Flutter'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: TextOutput(),
      ),
    );
  }
}