import 'package:flutter/material.dart';
import './textcontrol.dart';

class TextOutput extends StatefulWidget{
  @override
  _TextOutputState createState() => _TextOutputState();
}

class _TextOutputState extends State<TextOutput>{
  String msg = 'Jepara';

  void _changeText() {
    setState(() {
      if (msg.startsWith('P')) {
        msg = 'Jepara';
      }else{
        msg = 'Panggang';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Alamat Rumah Saya Di ', style: new TextStyle(fontSize:30.0),),
          TextControl(msg),
          RaisedButton(child: Text("alamat saya",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed:_changeText,),
        ],
      ),
    );
  }
}